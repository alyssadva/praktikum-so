# nomor 1
•	RAM
1.	Untuk melihat apa yang bisa dilakukan oleh ram, seperti panduan dalam penggunaan
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/ram/ram1.png)
2. Contoh tampilan dari free - -help seperti free - -lohi untuk melihat high dan low memori statistics
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/ram/eam2.png)

- CPU
1.	Ps - -help all untuk melihat apa saja yang dapat dilakukan oleh ps
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/cpu/cpu1.png)
2.	ps - -aux melihat proses yang sedang berjalan
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/cpu/cpu2.png)

- hardisk
1. df --help
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/hardisk/hardisk1.png)
2.	df -h untuk melihat statistik hardisk
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Monitoring%20resource/hardisk/hardis2.png)


# nomor 3
•	Mengakses sistem operasi pada jaringan menggunakan SSH

disini kita masuk kedalam server praktikum c menggunakan SSH dan port 22 lalu masukkan password server nya.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/jaringan%20menggunakan%20SSH/network1.png)

- memonitor program yang menggunakan network

netstat berfungsi untuk menampilkan statistik koneksi jaringan dari dan ke komputer yang sedang kita pakai.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/memonitor%20program/network_2.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/memonitor%20program/network3.png)

•	Mengoperasikan HTTP Client.

Curl merupakan singkatan dari “Client URL”. Curl command dibuat untuk mengecek konektivitas ke URL dan juga sebagai tool transfer data.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/Mengoperasikan%20HTTP%20Client/network4.png)

menggunakan wget untuk mengambil html nya

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/Mengoperasikan%20HTTP%20Client/httpclient.png)

html akan muncul disini

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20network/Mengoperasikan%20HTTP%20Client/client2.png)

# nomor 2
•	memonitor program yang sedang berjalan

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/memonitor%20program%20yang%20sedang%20berjalan/monitor_program.png)

•	menghentikan program yang sedang berjalan

1.	menggunakan command top

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/menghentikan%20program/berhenti1.png)

2.	untuk kill, klik keyword k karena k singkatan dari kill

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/menghentikan%20program/berhenti2.png)

Maka PID yang sedang berjalan 1
Klik 9 untuk menghentikan program secara paksa
Klik 15 untuk menghentikan program secara defaultt
Jika stoped menjadi angka 1 maka telah berhasil membuat program berhenti

Atau
menggunakan kill

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/menghentikan%20program/berhenti3.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/menghentikan%20program/berhenti4.png)

Kill <PID>, karena kita bukan owner maka menghentikan program menggunakan kill <PID> tidak bisa.

•	otomasi perintah menggunakan shell script

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/otomasi%20perintah/otomasi1.png)

1.	jalankan program shell script dengan ./alamat.sh untuk mengakses file shell script nya lalu isi paramater sesuai dengan isi shell script nya.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/otomasi%20perintah/otomasi2.png)

2.	Karena $3 merupakan folder dan $4 merupakan file sehingga isinya akan muncul disini berupa folder dan file

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/otomasi%20perintah/otomasi3.png)

•	Penjadwalan eksekusi program dengan cron
1.	Ketik crontab -e untuk mengedit isi crontab

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/crontab/cron1.png)

2.	Buat perintah nya pada 1217050009-alisa disini saya membuat file celana.log dimana file ini akan muncul pada directory 1217050009-alisa dalam satu menit

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/crontab/cron2.png)

3.	Celana.log akan muncul setelah satu menit pada halaman ini

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/manajemen%20program/crontab/cron3.png)

# nomor 4
•	Navigasi

cd

ls

pwd

• cd, untuk mengganti direktori 

• ls, menampilkan semua list file dan folder yang ada di direktori yang sedang ditempati 

• pwd, menampilkan direktori yang sedang ditempati

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/navigasi/navigasi.jpg)

•	CRUD file dan folder

mkdir

touch 

rm 

mv 

echo

• mkdir, untuk membuat direktori 

• touch, untuk membuat file 

• rm, menghapus file atau direktori 

• mv, mengubah nama folder

•	echo, untuk mengisi isi file 

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/CRUD/crud_1.jpg)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/CRUD/crud_2.jpg)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/CRUD/crud_3.jpg)

•	Manajemen ownership dan akses file dan folder

Ada dua command dasar dalam manajemen ownership yaitu chmod dan chown. 

Berikut ini adalah contoh penggunaannya: 

   chmod [option] 

• chmod, digunakan untuk memodifikasi permission suatu file. 

• [option] berisi perintah untuk mengubah permission 

• berisi nama file yang akan diubah permissionnya. 

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/ownership/owner1.jpg)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/ownership/owner2.jpg)

chown [user:group] 

• chown, digunakan untuk memodifikasi owner dari suatu file. 

• [user:group], berisi nama user/group mana yang akan menjadi ownernya. 

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/ownership/owner3.jpg)

Pada contoh di atas direktori Mobile Legends dimiliki oleh owner bernama praktikumc dan group bernama praktikumc.

   Mengubah ownership dari sebuah file atau folder dilakukan dengan perintah chown 
   
   <NAMA_OWNER>:<NAMA_GROUP> <NAMA_FILE_ATAU_FOLDER>
  
Mengubah owner dari folder. Arti perintah di bawah ini, folder Mobile Legends ownershipnya diubah menjadi sumedang dan group ownershipnya diubah menjadi rakyat

chown sumedang:rakyat Mobile-Legends

•	Pencarian file dan folder

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/pencarian/find_folder.jpg)

•	Kompresi data
1.	Buat file tar seperti dibawah ini, poin.tar sebagai nama dari file tar lalu mall merupakan file yang ingin dikompres dan disimpan didalam file poin.tar

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres1.png)

2.	Lalu akan muncul file poin.tar seperti gambar dibawah ini

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres2.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres3.png)

3.	Didalam file tar terdapat file mall yang akan di kompres

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres4.png)

4.	Lalu hapus file mall

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres5.png)

5.	Ketik xf untuk mengesktrak file nya, maka file mall akan muncul kembali setelah d ekstrak

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/Manajemen%20file%20dan%20folder/kompresi%20data/kompres6.png)

# nomor 5
1. Disini saya sudah berada pada folder saya yaitu 1217050009-alisa dan sudah membuat file shellscript seperti yang sudah dijelaskan dalam youtube, sehingga disini tinggal mengakses file tersebut

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/shell/shellscript1.png)

2. Disini saya menulis sebuah isi file seperti gambar dibawah ini dan membuat parameter 1 juga 2. Disini juga saya ingin menambahkan sebuah folder juga file yang nantinya ketika shell script ini di execute akan muncul folder dan file baru di folder saya sendri.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/shell/shellscript2.png)

3. lalu sebelum di execute, cek dahulu permisiion file tersebut. Disini file nya sudah diberi akses read write execute seperti yang sudah dijelaskan dalam youtube. Sehingga file ini sudah siap kita execute.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/shell/shellscript3.png)

4. saya menulis parameter satu yaitu alisa dan parameter 2 maka ketika dijalankan hasilnya akan seperti ini dan program tersebut sudah berjalan dengan sempurna

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/shell/shellscript4.png)

5. Jangan lupa untuk mengecek apakah folder dan file nya sudah bisa ditambahkan, disini folder sepatu dan kerudung sudah berhasil ditambahkan

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/shell/shellscript_5.png)

# nomor 6

Link youtube 

https://youtu.be/rAEIY1A3CeM

Screenshoot youtube nya

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/link%20yt%20shell%20script/1.png)

# nomor 7
Link maze game

https://maze.informatika.digital/maze/rupert/

Tampilan game

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game1.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game2.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game3.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/1_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/2_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/3_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/4_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/5_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/6_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/7_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/8_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/9_g.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game_4.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game5.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/game6.png)
![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/maze%20game/11_g.png)

