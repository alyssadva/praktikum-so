# 2. link web page / web service
http://135.181.26.148:25009

# 4. step membuat

masuk ke folder masing masing, buat folder sweather, di folder sweater buat file file 
yang dibutuhkan dengan perintah touch. lalu ls untuk mengetahui apakah file yang dibuat sudah
di create

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/1.png)

Isi File alyssadivaira.py dengan code python dengan cara ketik nvim (namafile). Ketika masuk menu nvim, tekan tombol I pada keyboard untuk mengedit kemudian setelah selesai mengedit tekan esc kemudian ketik “:wq” untuk menyimpan sekaligus keluar dari menu nvim

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/2.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/4.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/3.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/5.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/6.png)

gunakan perintah nvim untuk mengisi isi file Dockerfile

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/9.png)

FROM debian:bullseye-slim: Base image yang dipilih adalah Debian dengan tag bullseye-slim.

RUN apt-get update && apt-get install -y git neovim netcat python3-pip: apt-get install untuk menginstal git, neovim, netcat, dan python3-pip. Sedangkan apt-get update merupakan perintah untuk mengupdate daftar paket.

COPY . /home/sweather: mengCopy dari direktori lokal ke direktori /home/sweather di dalam container Docker.

WORKDIR /home/sweather: mengatur working directory di dalam container Docker menjadi /home/sweather.

RUN pip3 install --no-cache-dir -r requirements.txt: perintah pip3 install untuk menginstal semua dependensi yang diperlukan oleh aplikasi yang tercantum dalam file requirements.txt.

EXPOSE 25009`: port yang akan diexpose di dalam container Docker

CMD ["streamlit", "run", "--server.port=25009", "app.py"]: ketika container Docker dijalankan
maka perintah default akan dijalankan. Perintah tersebut adalah streamlit run --server.port=25009 app.py, yang akan menjalankan aplikasi Streamlit dengan port 25009.


requirements.txt ini berfungsi untuk menginstall dependensi yang dibutuhkan untuk file app.py

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/8.png)

isi juga docker-compose.yml nya

Dengan menggunakan docker-compose.yml ini, kita dapat menjalankan environment Docker dengan menjalankan perintah docker-compose up untuk membangun dan menjalankan container sesuai dengan konfigurasi yang diberikan.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/10.png)

`version: "3":` versi dari Docker Compose file menggunakan versi 3.

`services:`  mendefinisikan layanan (service) yang digunakan dalam environment Docker.

`sweather:` nama layanan yang diberikan kepada container yang akan dibangun dari image alyssadivaira/sweater:1.0

`image:` alyssadivaira/sweater:1.0: image yang digunakan untuk membangun container. 

`ports:` - "25009:25009": mapping port antara port host dan port container. Dalam hal ini, port 25009 akan di-mapping dari host ke port 25009 di dalam container.

`networks:` - niat-yang-suci: penggunaan jaringan yang sudah ada sebelumnya dengan nama niat-yang-suci. 

`volumes:` - /home/praktikumc/1217050009/sweather:/home/sweather: Ini menentukan mounting volume antara direktori host dengan direktori di dalam container. Dalam hal ini, direktori /home/praktikumc/1217050009/sweather pada host akan di-mount ke direktori /home/sweather di dalam container.

`networks`: niat-yang-suci: external: true: jaringan niat-yang-suci merupakan jaringan eksternal yang sudah ada sebelumnya.

lakukan Langkah berikut untuk membuat docker image

Dengan menjalankan perintah docker build -t sweather:1.0 ., Docker akan memproses Dockerfile yang ada di direktori saat ini dan membangun Docker image. Setelah itu, Docker image dapat digunakan untuk menjalankan container Docker.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/11.png)

`docker build`: perintah untuk membangun Docker image.
`-t sweather:1.0`: Opsi -t digunakan untuk memberikan tag (nama) pada Docker image yang akan dibangun. Tag ini dapat digunakan untuk mengidentifikasi versi atau label dari Docker image.

lakukan Langkah dibawah untuk mengecek apakah docker images sudah dibuat atau belum

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/12.png)

Dengan menjalankan perintah docker tag sweather:1.0 alyssadivaira/sweather:1.0, Docker akan membuat tag baru alyssadivaira/sweather:1.0 untuk Docker image.

Tag baru ini bisa berguna saat ingin mengunggah atau membagikan Docker image ke repository Docker yang berbeda atau saat ingin memberikan label kustom pada Docker image.

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/13.png)

login akun ke dockerhub dengan cara seperti gambar dibawah

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/14.png)

setelah login, push repository yang sudah dibuat di docker images ke repository dockerhub anda 

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/15.png)

cek di repository dockerhub tadi apakah images sudah di push atau belum

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/24.png)

kemudian run dengan cara docker-compose up dan docker image akan berubah menjadi container yang menjalankan aplikasi dengan non-stop sampai server dimatikan 

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/16.png)

Jika terjadi seperti gambar dibawah ini artinya container tidak berjalan karena default container adalah menjalankan program kemudian langsung berhenti kembali

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/17.png)

Untuk menyelesaikan masalah sebelumnya, caranya cukup mudah yaitu jalankan perintah docker-compose up –d. –d ini berfungsi untuk menjalankan program di latar belakang secara terus menerus

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/18.png)

kemudian ketikan url https://(server):(port)

http://135.181.26.148:25009

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/19.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/20.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/21.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/22.png)

![](https://gitlab.com/alyssadva/praktikum-so/-/raw/main/UAS/ss%20moba/23.png)


# 5 
 - Deskripsi produk
 
Disini saya membuat project Web page cuaca, web page cuaca ini digunakan untuk memberikan informasi aktual tentang kondisi cuaca di suatu lokasi tertentu. Kegunaan utama dari web page cuaca adalah untuk membantu pengguna memperoleh informasi tentang suhu, kelembaban, kondisi cuaca saat ini, serta perkiraan cuaca untuk beberapa waktu ke depan di lokasi yang diminati. Kegunaan lainnya dari web page diantaranya perencanaan aktivitas, Web page cuaca memungkinkan pengguna untuk merencanakan aktivitas sehari-hari dengan memperhatikan kondisi cuaca. Misalnya, pengguna dapat memeriksa apakah hari ini akan hujan atau cerah, sehingga dapat memutuskan untuk membawa payung atau memilih aktivitas indoor.

project ini menggunakan library streamlit dengan bahasa python dan dataset nya menggunakan dataset online pada alamat https://api.openweathermap.org/data/2.5/weather
kemudian dipanggil di dalam alyssadivaira.py untuk menampilkan dataset kedalam webpage

 - Bagaimana sistem operasi digunakan dalam proses containerization

Sistem operasi memainkan peran penting dalam proses containerization. Containerization adalah praktik mengemas aplikasi dan dependensinya ke dalam unit terisolasi yang disebut "kontainer". Kontainer menyediakan lingkungan yang terisolasi untuk menjalankan aplikasi, yang memungkinkan portabilitas dan skalabilitas yang lebih baik.

Sistem operasi digunakan dalam proses containerization dengan menggunakan fitur-fitur yang disediakan oleh sistem operasi untuk mengisolasi kontainer satu sama lain dan host. Beberapa komponen kunci dari sistem operasi yang digunakan dalam containerization meliputi:

1. Namespace: Sistem operasi menyediakan mekanisme yang disebut namespace, yang memungkinkan isolasi sumber daya sistem seperti proses, jaringan, file sistem, dan pengguna antara kontainer. Setiap kontainer memiliki namespace yang terisolasi, sehingga kontainer-kontainer tersebut tidak saling berinterferensi.

2. Cgroups (Control groups): Sistem operasi juga menyediakan fitur yang disebut cgroups, yang memungkinkan pembatasan dan alokasi sumber daya seperti CPU, memori, dan bandwidth jaringan kepada kontainer. Dengan menggunakan cgroups, kita dapat membatasi penggunaan sumber daya setiap kontainer sesuai kebutuhan.

3. File sistem overlay: Sistem operasi memungkinkan penggunaan file sistem overlay, yang memungkinkan kontainer untuk menggunakan file sistem yang bersifat "read-only" dan menambahkan layer file sistem yang bersifat "read-write" di atasnya. Ini memungkinkan kontainer untuk berbagi sumber daya yang sama dan mengurangi overhead penyimpanan.

4. Image container: Sistem operasi mendukung pembuatan dan manajemen image container. Image container adalah template yang berisi semua dependensi dan konfigurasi yang diperlukan untuk menjalankan kontainer. Sistem operasi menyediakan alat untuk membuat, menyimpan, dan mengelola image container ini.

Dengan memanfaatkan fitur-fitur ini, sistem operasi memungkinkan pengguna untuk menjalankan kontainer secara terisolasi satu sama lain, dengan penggunaan sumber daya yang terbatas, dan dengan tingkat portabilitas yang tinggi. Ini membuat proses containerization menjadi lebih efisien dan dapat diandalkan dalam pengembangan dan pengoperasian aplikasi.

 - bagaimana containerization dapat membantu mempermudah pengembangan aplikasi

 Containerization dapat membantu mempermudah pengembangan aplikasi dalam beberapa cara:

1. Isolasi Lingkungan: Dalam container, aplikasi dan dependensinya diisolasi dari lingkungan host dan kontainer lainnya. Hal ini memungkinkan pengembang untuk menghindari konflik dependensi dan masalah kompatibilitas yang mungkin terjadi pada lingkungan pengembangan yang berbeda. Dengan menggunakan container, pengembang dapat memastikan bahwa aplikasi mereka berjalan secara konsisten di berbagai lingkungan.

2. Portabilitas: Container adalah unit terstandarisasi yang terdiri dari semua dependensi yang diperlukan. Ini memungkinkan pengembang untuk mengemas aplikasi mereka beserta dependensinya ke dalam container dan menjalankannya di berbagai platform dan sistem operasi tanpa perlu memodifikasi kode aplikasi. Dengan menggunakan container, pengembang dapat memastikan bahwa aplikasi mereka dapat dijalankan dengan lancar di berbagai lingkungan yang berbeda.


3. Skalabilitas: Container memungkinkan pengembang untuk dengan mudah memperluas aplikasi mereka dengan mengelola replika kontainer yang berjalan di beberapa mesin fisik atau dalam lingkungan yang dikelola secara terpusat seperti Kubernetes. Dengan menggunakan alat manajemen container yang tepat, pengembang dapat menangani penyebaran dan penurunan skala aplikasi secara efisien dan otomatis.


Dengan demikian, containerization membantu mempermudah pengembangan aplikasi dengan menyediakan lingkungan yang terisolasi, portabilitas yang tinggi, dan alat untuk mengelola dan mengoptimalkan aplikasi dalam berbagai skenario. Ini memungkinkan pengembang untuk fokus pada pengembangan aplikasi itu sendiri, sementara aspek infrastruktur dikemas dan diatur dengan baik melalui container.

 - Apa itu DevOps dan bagaimana DevOps membantu pengembangan aplikasi

DevOps adalah pendekatan atau praktik yang menggabungkan pengembangan (Development) dan operasi (Operations) dalam proses pengembangan perangkat lunak. Tujuan utama dari DevOps adalah untuk mencapai kolaborasi yang lebih baik antara tim pengembang perangkat lunak dan tim operasi TI, dengan tujuan meningkatkan kecepatan, kualitas, dan keamanan dalam pengembangan aplikasi.

DevOps melibatkan adopsi otomatisasi dan integrasi alur kerja (workflow) yang memungkinkan pengembang dan operator bekerja bersama-sama secara lebih efektif. 

Beberapa cara di mana DevOps membantu pengembangan aplikasi meliputi:

1. Kontinuitas Integrasi dan Pengiriman (Continuous Integration and Delivery/CI/CD): DevOps mendorong adopsi CI/CD, yang merupakan praktik otomatisasi pengujian, penggabungan kode, dan penerapan aplikasi. Dengan CI/CD, perubahan kode dapat secara otomatis diuji dan diintegrasikan ke dalam kode yang ada, dan aplikasi dapat dengan mudah diterapkan ke dalam lingkungan produksi dengan risiko yang lebih rendah.

2. Skalabilitas dan Resiliensi: DevOps membantu meningkatkan skalabilitas dan resiliensi aplikasi dengan memperkenalkan otomatisasi penyebaran dan manajemen infrastruktur. Tim operator dapat menggunakan alat otomatis untuk mengelola sumber daya IT, meningkatkan elastisitas, dan memastikan bahwa aplikasi berjalan dengan baik bahkan dalam situasi beban yang tinggi atau kegagalan sistem.

3. Peningkatan Kualitas: DevOps mendorong penggunaan pengujian otomatis dan praktik pengujian terus menerus dalam siklus pengembangan. Dengan pengujian otomatis yang terintegrasi, bug dapat dideteksi lebih awal, dan masalah dapat diperbaiki lebih cepat sebelum aplikasi diimplementasikan ke dalam produksi.

Secara keseluruhan, DevOps membantu meningkatkan efisiensi, kecepatan, kualitas, dan keamanan dalam pengembangan aplikasi.

 - Contoh penerapan kasus DevOps di perusahaan Grab

Sebagai perusahaan teknologi besar, Grab telah mengadopsi praktik DevOps dalam pengembangan dan pengelolaan aplikasinya. Berikut adalah beberapa contoh implementasi DevOps di Grab:

1. Integrasi Continuous Integration/Continuous Delivery (CI/CD): Grab menggunakan alat CI/CD seperti Jenkins dan GitLab CI untuk mengotomatiskan alur kerja pengembangan perangkat lunak. Setiap kali ada perubahan kode, sistem secara otomatis memicu pengujian otomatis dan penerapan aplikasi ke lingkungan produksi. Ini memungkinkan tim Grab untuk mengirim perubahan dengan cepat dan secara teratur, mempercepat waktu penyebaran fitur baru.

2. Containerization: Grab menggunakan teknologi kontainer seperti Docker dan Kubernetes untuk mengemas dan mengelola aplikasinya. Dengan menggunakan kontainer, Grab dapat mengisolasi aplikasi dan dependensinya dalam lingkungan yang terstandarisasi, memastikan portabilitas yang lebih baik dan kemudahan dalam penyebaran ke berbagai lingkungan.

3. Automatisasi Infrastruktur: Grab menerapkan praktik Infrastructure as Code (IaC) dengan menggunakan alat seperti Terraform atau Ansible. Hal ini memungkinkan tim Grab untuk mengotomatiskan penyebaran dan pengelolaan infrastruktur mereka dengan mengkodekan konfigurasi dan kebijakan infrastruktur dalam file. Dengan demikian, tim Grab dapat dengan cepat dan konsisten membuat dan mengelola sumber daya infrastruktur yang diperlukan.

4. Pemantauan dan Analisis: Grab menggunakan berbagai alat pemantauan seperti Prometheus, Grafana, dan ELK Stack untuk memantau kesehatan dan kinerja aplikasi mereka. Data pemantauan digunakan untuk mendapatkan wawasan tentang kinerja aplikasi, mengidentifikasi masalah, dan mengoptimalkan kinerja sistem.

5. Pengujian Otomatis: Grab mengadopsi pendekatan pengujian otomatis dengan menggunakan kerangka kerja pengujian seperti Selenium atau Appium. Pengujian otomatis memungkinkan Grab untuk secara efisien menguji fungsionalitas dan kinerja aplikasinya, serta mendeteksi masalah atau bug lebih awal dalam siklus pengembangan.

6. DevSecOps: Grab memperhatikan keamanan dalam praktik DevOps mereka dengan menerapkan pendekatan DevSecOps. Mereka mengintegrasikan pemindaian keamanan otomatis ke dalam alur kerja CI/CD mereka, serta menerapkan praktik pengelolaan akses dan kebijakan keamanan yang ketat dalam infrastruktur mereka.

Implementasi DevOps di Grab terus berkembang dan disesuaikan dengan kebutuhan perusahaan serta perkembangan terbaru dalam industri. Dengan mengadopsi praktik DevOps, Grab dapat mengoptimalkan pengembangan aplikasi mereka, meningkatkan kecepatan penyebaran fitur, dan memberikan pengalaman yang lebih baik kepada pengguna mereka.

# Link youtube

https://youtu.be/qcO5Dlzoqhs
