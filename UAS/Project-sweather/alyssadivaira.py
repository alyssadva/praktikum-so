import streamlit as st
import requests

API_KEY = "1fe5f03e8b679377cbc41601289edfdd"
API_URL = "https://api.openweathermap.org/data/2.5/weather"

def get_weather_data(city):
    params = {
        "q": city,
        "appid": API_KEY,
        "units": "metric"
    }
    response = requests.get(API_URL, params=params)
    data = response.json()
    return data

def main():
    st.set_page_config(page_title="Sweather", page_icon=":partly_sunny:")
    st.title("Sweather")
    st.markdown(
        """
        <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f8f8f8;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #ffffff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .title {
            text-align: center;
            font-size: 32px;
            font-weight: bold;
            margin-bottom: 30px;
            color: #333333;
        }

        .input-container {
            margin-bottom: 20px;
        }

        .input-label {
            font-weight: bold;
            margin-bottom: 5px;
            color: #333333;
        }

        .input-field {
            padding: 10px;
            width: 100%;
            border: 2px solid #dddddd;
            border-radius: 5px;
            font-size: 16px;
            outline: none;
        }

        .button {
            padding: 10px 20px;
            background-color: #333333;
            color: #ffffff;
            border: none;
            border-radius: 5px;
            font-size: 16px;
            cursor: pointer;
        }

        .button:hover {
            background-color: #555555;
        }

        .weather-info {
            margin-top: 30px;
            padding: 20px;
            border-radius: 5px;
            background-color: #ffffff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .weather-info h3 {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 20px;
            color: #333333;
        }

        .weather-info p {
            font-size: 18px;
            margin-bottom: 10px;
            color: #555555;
        }

        .error-message {
            text-align: center;
            color: #ff0000;
            font-weight: bold;
        }
        </style>
        
        """,
        unsafe_allow_html=True
    )

    city = st.text_input("Masukkan nama kota")

    if st.button("Cek Cuaca"):
        if city:
            weather_data = get_weather_data(city)
            if weather_data["cod"] == 200:
                st.subheader(f"Cuaca saat ini di {city}:")
                temperature = weather_data["main"]["temp"]
                st.write(f"Suhu: {temperature}°C")
                feels_like = weather_data["main"]["feels_like"]
                st.write(f"Sensasi Suhu: {feels_like}°C")
                humidity = weather_data["main"]["humidity"]
                st.write(f"Kelembapan: {humidity}%")
                description = weather_data["weather"][0]["description"].capitalize()
                st.write(f"Deskripsi: {description}")
                wind_speed = weather_data["wind"]["speed"]
                st.write(f"Kecepatan Angin: {wind_speed} m/s")
            else:
                st.error("Data cuaca tidak ditemukan.")
        else:
            st.warning("Masukkan nama kota terlebih dahulu.")

if __name__ == "__main__":
    main()


